//
//  NetworkManager.swift
//  Daily Flights
//
//  Created by Martin Kompan on 15/12/2022.
//

import MKUseful
import Foundation

extension NetworkManager {
    static func getHeaders() -> [String:String]? {
        ["accept":"json/application"
         , "apikey":"dXVJHKhrEV5zz_1o09C3dphCCWyNc84U"]
    }
    
    static func getFlights(completion: @escaping ([FlightResponse]) -> Void) {
        let params = GetFlightsRequestParams()
        var components = AppSettings.URL.getFlights
        
        do {
            let jsonData = try JSONEncoder().encode(params)
            let jsonReq = try JSONSerialization.jsonObject(with: jsonData) as! [String: Any]
            components.queryItems = jsonReq.map { URLQueryItem(name: $0, value: "\($1)") }
        } catch {
            NSLog("Request body parsing error for path: \(AppSettings.URL.getFlights)")
        }
        
        NetworkManager.getData(components.string ?? "", headers: getHeaders()) { data in
            guard let data = data else {
                completion([])
                return
            }
            
            do
            {
                let result = try JSONDecoder ().decode (GetFlightsReasponseData.self, from: data)
                
                completion(result.data ?? [])
            } catch (let error) {
                print(error.localizedDescription)
                completion([])
            }
        }
    }
}
