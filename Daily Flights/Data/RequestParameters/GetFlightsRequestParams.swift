//
//  GetFlightsRequestParams.swift
//  Daily Flights
//
//  Created by Martin Kompan on 13/12/2022.
//

import Foundation
import MKUseful

struct GetFlightsRequestParams: Encodable {
    
    let date_from = Date.todayWithFormat("dd/MM/YYYY")
    
    let date_to = Calendar.current.date(byAdding: .day, value: 1, to: Date())?.stringWithFormat("dd/MM/YYYY") ?? ""
    
    let fly_from = "PRG"// getFrom() // TODO: get exact location from iOS device !!!
    
    let limit = 10
    
    static func getFrom() -> String{
        
        // TODO: get exact location from iOS device !!!
        
        //  49.2-16.61-250km
        
        // fly_from = -23.24--47.86-500km
        
        var countryCode: String?
        
        countryCode = NSLocale.current.language.region?.identifier
        
        return countryCode ?? ""
    }
}
