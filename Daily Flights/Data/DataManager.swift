//
//  DataManager.swift
//  Daily Flights
//
//  Created by Martin Kompan on 15/12/2022.
//

import MKUseful
import Foundation

public extension DataManager {
    
    static var lastUpdateOld: Bool {
        
        if let lastUpdate = UserDefaults.standard.object(forKey: AppSettings.UserDefaultsKeys.lastUpdate) as? Date {
            return Calendar.current.isDateInYesterday(lastUpdate) || ((Calendar.current.dateComponents([.hour], from: lastUpdate, to: Date()).hour ?? 30 ) > 24)
        }
        
        return true
    }
    
    static func getFlights(completion: @escaping ([Flight]?) -> Void) { shared.getData(Flight.self, completion: completion) }
}
