//
//  Flight+CoreDataClass.swift
//  Daily Flights
//
//  Created by Martin Kompan on 13/12/2022.
//
//

import Foundation
import CoreData
import SwiftUI

@objc(Flight)
public class Flight: NSManagedObject {
    
    convenience init(new: FlightResponse, context: NSManagedObjectContext) {
        self.init(context: context)
        
        self.cityFrom = new.cityFrom
        self.cityTo = new.cityTo
        self.countryFrom = new.countryFrom
        self.countryTo = new.countryTo
        self.countryToCode = new.countryToCode
        self.dTime = new.dTime
        self.dTimeUTC = new.dTimeUTC
        self.aTime = new.aTime
        self.aTimeUTC = new.aTimeUTC
        self.deepLink = new.deepLink
        self.price = Float(new.price ?? 0.0)
        self.bagsPriceOne = Float(new.bagsPriceOne ?? 0.0)
        self.bagsPriceTwo = Float(new.bagsPriceTwo ?? 0.0)
    }
    
    // Detail View's titles
    let fromTitle = "Flight From: "
    let toTitle = "Fity To: "
    let dTimeTitle = "Departure Time: "
    let aTimeTitle = "Arrival Time: "
    let deepLinkTitle = "Flight's web page"
    let priceTitle = "Price: "
    let bagsPriceTitle = "Bags Prices: "
    let shareButtonName = "square.and.arrow.up.circle"
    
    var imagePath:String {
        AppSettings.URL.image + "\((cityTo ?? "").lowercased().replacingOccurrences(of: " ", with: "-"))_\((countryToCode ?? "").lowercased()).jpg"
    }
}
