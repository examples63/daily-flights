//
//  Flight+CoreDataProperties.swift
//  Daily Flights
//
//  Created by Martin Kompan on 13/12/2022.
//
//

import Foundation
import CoreData


extension Flight {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Flight> {
        return NSFetchRequest<Flight>(entityName: "Flight")
    }
    
    // Core Data properties
    
    @NSManaged public var cityFrom: String?
    @NSManaged public var cityTo: String?
    @NSManaged public var countryFrom: String?
    @NSManaged public var countryTo: String?
    @NSManaged public var countryToCode: String?
    @NSManaged public var dTime: String?
    @NSManaged public var dTimeUTC: String?
    @NSManaged public var aTime: String?
    @NSManaged public var aTimeUTC: String?
    @NSManaged public var deepLink: String?
    @NSManaged public var price: Float
    @NSManaged public var bagsPriceOne: Float
    @NSManaged public var bagsPriceTwo: Float
    
}

extension Flight : Identifiable {
    
}
