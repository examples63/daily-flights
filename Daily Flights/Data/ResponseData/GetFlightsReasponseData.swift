//
//  GetFlightsReasponseData.swift
//  Daily Flights
//
//  Created by Martin Kompan on 16/12/2022.
//

import Foundation

struct GetFlightsReasponseData: Decodable {
    
    let search_id: String?
    let currency: String?
    let fx_rate: Double?
    let data: [FlightResponse]?
    
    private enum CodingKeys: String, CodingKey {
        case search_id
        case currency
        case fx_rate
        case data
    }
    
    public init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        search_id = try values.decode(String.self, forKey: .search_id)
        currency = try values.decode(String.self, forKey: .currency)
        UserDefaults.standard.set(currency, forKey: AppSettings.UserDefaultsKeys.currency)
        fx_rate = try values.decode(Double.self, forKey: .fx_rate)
        data = try values.decode([FlightResponse].self, forKey: .data)
    }
}

struct FlightResponse: Decodable {
    
    var cityFrom: String?
    var cityTo: String?
    var countryFrom: String?
    var countryTo: String?
    var countryToCode: String?
    var dTime: String?
    var dTimeUTC: String?
    var aTime: String?
    var aTimeUTC: String?
    var deepLink: String?
    var price: Double?
    var bagsPriceOne: Double?
    var bagsPriceTwo: Double?
    var bagsPrice: BagsPrice?
    var country: Country?
    
    private enum CodingKeys: String, CodingKey {
        case cityFrom
        case cityTo
        case countryFrom
        case countryTo
        case dTime = "local_departure"
        case dTimeUTC = "utc_departure"
        case aTime = "local_arrival"
        case aTimeUTC = "utc_arrival"
        case bagsPrice = "bags_price"
        case price
        case deepLink = "deep_link"
    }
    
    public  init(from decoder:Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        cityFrom = try values.decode(String.self, forKey: .cityFrom)
        cityTo = try values.decode(String.self, forKey: .cityTo)
        country = try values.decode(Country.self, forKey: .countryFrom)
        countryFrom = country?.name
        country = try values.decode(Country.self, forKey: .countryTo)
        countryTo = country?.name
        countryToCode = country?.code
        dTime = try values.decode(String.self, forKey: .dTime)
        dTimeUTC = try values.decode(String.self, forKey: .dTimeUTC)
        aTime = try values.decode(String.self, forKey: .aTime)
        aTimeUTC = try values.decode(String.self, forKey: .aTimeUTC)
        bagsPrice = try values.decode(BagsPrice.self, forKey: .bagsPrice)
        bagsPriceOne = bagsPrice?.one ?? 0.0
        bagsPriceTwo = bagsPrice?.two ?? 0.0
        price = try values.decode(Double.self, forKey: .price)
        deepLink = try values.decode(String.self, forKey: .deepLink)
    }
    
    // Nested paths:
    
    struct BagsPrice: Decodable {
        var one: Double?
        var two: Double?
        
        private enum CodingKeys: String, CodingKey {
            case one = "1"
            case two = "2"
        }
        
        public init(from decoder:Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            
            one = try? values.decode(Double.self, forKey: .one)
            two = try? values.decode(Double.self, forKey: .two)
        }
    }
    
    struct Country: Decodable {
        var name: String?
        var code: String?
        
        private enum CodingKeys: String, CodingKey {
            case name
            case code
        }
        
        public init(from decoder:Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            
            name = try? values.decode(String.self, forKey: .name)
            code = try? values.decode(String.self, forKey: .code)
        }
    }
}
