//
//  FlightListViewModel.swift
//  Daily Flights
//
//  Created by Martin Kompan on 12/12/2022.
//

import SwiftUI
import CoreData

extension FlightListView {
    
    @MainActor class ViewModel: ObservableObject {
        
        // MARK: Stored Properties
        
        @Published  var flightsList = [Flight]()
        @Published private(set) var detailButtonTitle = ""
        
        private var model = FlightListModel()
        
        func initFRC(context moc: NSManagedObjectContext) {
            model.initFRC(context: moc, flightsCallback: { self.flightsList = $0 })
            self.detailButtonTitle = model.detailButtonTitle
        }
        
        // MARK: Class Methods
        
        func fetchFlights() {
            model.fetchFlights()
        }
    }
}
