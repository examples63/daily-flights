//
//  FlightListView.swift
//  Daily Flights
//
//  Created by Martin Kompan on 12/12/2022.
//

import SwiftUI

struct FlightListView: View {
    
    // MARK: Stored Properties
    
    @Environment(\.managedObjectContext) var moc
    
    @StateObject private var viewModel = ViewModel()
    
    @State var path = NavigationPath()
    
    @State private var selectedTab = 0
    
    // MARK: Views
    
    var body: some View {
        NavigationStack(path: $path) {
            TabView(selection: $selectedTab){
                ForEach(viewModel.flightsList) { flight in
                    pageView(flight: flight)
                }
            }
            .tabViewStyle(.page)
            .onAppear(perform: {
                viewModel.initFRC(context: moc)
            })
            .toolbar(.hidden)
            .navigationDestination(for: Flight.self) { flight in
                FlightDetailView(model: flight)
            }
        }
    }
    
    func pageView(flight:Flight) -> some View {
        VStack{
            AsyncImage(url: URL(string: flight.imagePath)) { image in
                image
                    .resizable()
                    .aspectRatio(contentMode: .fit)
            } placeholder: {
                ProgressView()
                    .progressViewStyle(.circular)
            }
            Spacer()
            HStack {
                Text((flight.cityTo ?? "") + ", " + (flight.countryTo ?? "")).padding(.leading, 20).font(.system(size: 20, weight: Font.Weight.bold))
                Spacer()
            }.padding(12)
            HStack {
                Text(flight.priceTitle).padding(.leading, 20)
                Text(String(flight.price) + "  " + (UserDefaults.standard.string(forKey: AppSettings.UserDefaultsKeys.currency) ?? "")).padding(.trailing, 20)
                Spacer()
            }.padding(12)
            HStack {
                Link(flight.deepLinkTitle, destination: URL(string: flight.deepLink ?? "")!).fontWeight(.bold).padding(.leading, 20)
                Spacer()
                NavigationLink(value: flight){
                    Text(viewModel.detailButtonTitle).padding(.trailing, 20)
                }
            }.padding(12).padding(.bottom, 20)
            
        }
    }
}
