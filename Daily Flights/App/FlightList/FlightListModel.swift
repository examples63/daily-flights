//
//  FlightListModel.swift
//  Daily Flights
//
//  Created by Martin Kompan on 12/12/2022.
//

import Foundation
import MKUseful
import CoreData
import SwiftUI

class FlightListModel: NSObject, NSFetchedResultsControllerDelegate {
    
    private var flightFRC = NSFetchedResultsController<Flight>()
    private var listCallback: (([Flight]) -> Void)?
    private var moc: NSManagedObjectContext?
    
    let detailButtonTitle = "Show Details"
    
    private var lastUpdateOld: Bool {
        if let lastUpdate = UserDefaults.standard.object(forKey: AppSettings.UserDefaultsKeys.lastUpdate) as? Date {
            return Calendar.current.isDateInYesterday(lastUpdate) || ((Calendar.current.dateComponents([.hour], from: lastUpdate, to: Date()).hour ?? 30 ) > 24)
        }
        return true
    }
    
    func initFRC(context moc: NSManagedObjectContext, flightsCallback: @escaping ([Flight]) -> Void) {
        listCallback = flightsCallback
        self.moc = moc
        
        let fetchRequest: NSFetchRequest<Flight> = Flight.fetchRequest()
        fetchRequest.sortDescriptors = []
        
        flightFRC = NSFetchedResultsController(fetchRequest: fetchRequest,
                                               managedObjectContext: moc, sectionNameKeyPath: nil, cacheName: nil)
        
        flightFRC.delegate = self
        self.fetchFlights()
    }
    
    func fetchFlights() {
        try? flightFRC.performFetch()
        let flightsList = flightFRC.fetchedObjects ?? []
        
        if (flightsList.count == 0 || lastUpdateOld) {
            try? moc?.deleteAll(Flight.self)
            
            NetworkManager.getFlights{
                var flights:[Flight] = []
                
                $0.forEach {
                    if let m = self.moc {
                        let new = Flight(new: $0, context: m)
                        flights.append(new)
                    }
                }
                
                try? self.moc?.save();
                UserDefaults.standard.set(Date(), forKey: AppSettings.UserDefaultsKeys.lastUpdate)
                self.listCallback?(flights) }
        }
        listCallback?(flightsList)
    }
    
    // MARK: Delegate methods
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        if controller == flightFRC {
            let flightsList = flightFRC.fetchedObjects ?? []
            listCallback?(flightsList)
        }
    }
}
