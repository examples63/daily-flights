//
//  FlightDetailViewModel.swift
//  Daily Flights
//
//  Created by Martin Kompan on 16/12/2022.
//

import Foundation

extension FlightDetailView {
    @MainActor class ViewModel: ObservableObject {
        
        // MARK: Stored Properties
        
        private var model:Flight? {
            didSet{
                cityFrom = model?.cityFrom ?? ""
                cityTo = model?.cityTo ?? ""
                countryFrom = (model?.countryFrom ?? "")
                countryTo = (model?.countryTo ?? "") + " " + (model?.countryToCode ?? "")
                dTime = model?.dTime ?? ""
                aTime = model?.aTime ?? ""
                deepLink = model?.deepLink  ?? ""
                price = String(format: "%.2f ", model?.price ?? "") + (UserDefaults.standard.string(forKey: AppSettings.UserDefaultsKeys.currency) ?? "")
                bagsPrice = String(format: "%.2f   ", model?.bagsPriceOne ?? "") + String(format: "%.2f   ", model?.bagsPriceTwo ?? "")
            }
        }
        
        @Published private(set) var cityFrom = String()
        @Published private(set) var cityTo = String()
        @Published private(set) var countryFrom = String()
        @Published private(set) var countryTo = String()
        @Published private(set) var dTime = String()
        @Published private(set) var dTimeUTC = String()
        @Published private(set) var aTime = String()
        @Published private(set) var aTimeUTC = String()
        @Published private(set) var deepLink = String()
        @Published private(set) var price = String()
        @Published private(set) var bagsPrice = String()
        
        // Detail View's titles
        
        @Published private(set) var fromTitle = String()
        @Published private(set) var toTitle = String()
        @Published private(set) var dTimeTitle = String()
        @Published private(set) var aTimeTitle = String()
        @Published private(set) var deepLinkTitle = String()
        @Published private(set) var priceTitle = String()
        @Published private(set) var bagsPriceTitle = String()
        @Published private(set) var shareButtonName = String()
        
        // MARK: Class Methods
        
        func update(model: Flight) {
            self.model = model
            
            self.fromTitle = model.fromTitle
            self.toTitle = model.toTitle
            self.dTimeTitle = model.dTimeTitle
            self.aTimeTitle = model.aTimeTitle
            self.deepLinkTitle = model.deepLinkTitle
            self.priceTitle = model.priceTitle
            self.bagsPriceTitle = model.bagsPriceTitle
            self.shareButtonName = model.shareButtonName
        }
    }
}
