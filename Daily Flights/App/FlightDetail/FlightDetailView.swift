//
//  FlightDetailView.swift
//  Daily Flights
//
//  Created by Martin Kompan on 16/12/2022.
//

import SwiftUI

struct FlightDetailView: View {
    
    // MARK: Stored Properties
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    @StateObject private var viewModel = ViewModel()
    
    @State var shareText: ShareText?
    
    let model:Flight
    
    // MARK: Views
    
    var body: some View {
        
        VStack{
            if viewModel.cityFrom != "" {
                if viewModel.countryFrom != "" {
                    getInfoView(viewModel.cityFrom + ", " + viewModel.countryFrom, title: viewModel.fromTitle)
                } else {
                    getInfoView(viewModel.cityFrom, title: viewModel.fromTitle)
                }
            }
            if (viewModel.cityTo != ""){
                if viewModel.countryTo != "" && viewModel.countryTo != " " {
                    getInfoView(viewModel.cityTo + ", " + viewModel.countryTo, title: viewModel.toTitle)
                } else {
                    getInfoView(viewModel.cityTo, title: viewModel.toTitle)
                }
            }
            if(viewModel.dTime != ""){
                getInfoView(viewModel.dTime, title: viewModel.dTimeTitle)
            }
            if(viewModel.aTime != ""){
                getInfoView(viewModel.aTime, title: viewModel.aTimeTitle)
            }
            if(viewModel.price != ""){
                getInfoView(viewModel.price, title: viewModel.priceTitle)
            }
            if(viewModel.bagsPrice != ""){
                let bPrice = viewModel.bagsPrice + (UserDefaults.standard.string(forKey: AppSettings.UserDefaultsKeys.currency) ?? "")
                getInfoView(bPrice, title: viewModel.bagsPriceTitle)
            }
            if(viewModel.deepLink != ""){
                Link(viewModel.deepLinkTitle, destination: URL(string: viewModel.deepLink)!).padding(20).fontWeight(.bold)
            }
            Spacer()
        }.scrollDisabled(false)
            .onAppear(perform: { viewModel.update(model: model) } )
            .toolbar(.visible)
        // TODO: share button action fix !
//            .toolbar {
//                if(viewModel.deepLink != ""){
//                ToolbarItem(placement: .navigationBarTrailing) {
//                    Button(action: {
//                        shareText = ShareText(text: viewModel.deepLink)
//                    }, label: {
//                        Image(systemName: viewModel.shareButtonName)
//                    })
//                }
//                }
//            }
    }
    
    func getInfoView(_ value:String, title:String) -> some View {
        VStack{
            HStack {
                Text(title).padding(.leading, 20)
                Spacer()
            }.padding(8)
            HStack {
                Spacer()
                Text(value).padding(.trailing, 20)
            }.padding(8)
        }
    }
    
    // Share Activity View
    struct ActivityView: UIViewControllerRepresentable {
        let text: String
        
        func makeUIViewController(context: UIViewControllerRepresentableContext<ActivityView>) -> UIActivityViewController {
            return UIActivityViewController(activityItems: [text], applicationActivities: nil)
        }
        
        func updateUIViewController(_ uiViewController: UIActivityViewController, context: UIViewControllerRepresentableContext<ActivityView>) {}
    }
    
    // @State Share Text
    struct ShareText: Identifiable {
        let id = UUID()
        let text: String
    }
}

