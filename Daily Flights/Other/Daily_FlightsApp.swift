//
//  Daily_FlightsApp.swift
//  Daily Flights
//
//  Created by Martin Kompan on 15/10/2022.
//

import SwiftUI
import MKUseful

@main
struct Daily_FlightsApp: App {
    
    // MARK: Stored Properties
    
    let persistenceController = PersistenceController.shared
    
    // MARK: Scenes
    
    var body: some Scene {
        WindowGroup {
            FlightListView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
