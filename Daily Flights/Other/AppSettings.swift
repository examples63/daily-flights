//
//  DFAppSettings.swift
//  Daily Flights
//
//  Created by Martin Kompan on 06/12/2022.
//

import Foundation

struct AppSettings {
    
    struct URL {
        
        static let scheme = "https"
        
        static let host = "api.tequila.kiwi.com"
        
        static let apikey = "dXVJHKhrEV5zz_1o09C3dphCCWyNc84U" 
        
        static let image = "https://images.kiwi.com/photos/600x600/"
        
        static var getFlights:URLComponents {
            var components = URLComponents()
            components.scheme = scheme
            components.host = host
            components.path = "/v2/search" 
            
            return components
        }
    }
    
    struct UserDefaultsKeys {
        static let currency = "last_update_currency"
        static let lastUpdate = "last_update_time"
    }
}
