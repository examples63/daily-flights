# Daily Flights Swift

[**Author:** Martin Kompan](https://gitlab.com/examples63/mkuseful/-/blob/main/certs/Programming_certificates.md)

Tested using **Xcode 14.1**

Using: **Swift 5.7**

Architecture: **MVVMO** (MVVM + Observable pattern)

Using: **SwiftUI**, **Combine/Concurrency**, **only native iOS SDK !** (no 3rd party libraries allowed, although I used my own native library [MKUseful](https://gitlab.com/examples63/mkuseful) )

iOS deployment target: **16.0**

## TODOs 

1. get exact location from iOS device ( GetFlightsRequestParams )
2. share button action fix ! (Flight detail screen)

## Example task description

shows 5 interesting flights to destinations you can visit with Kiwi.com
- day offers are limited to 5 per day and should not be repeating, that means that on the next day the app will display different set of flights/destinations


void using 3rd party libraries

SwiftUI
Combine/Concurrency


to get list of popular flights to select from, you can query the following API:
- https://api.skypicker.com/flights?v=3&sort=popularity&asc=0&locale=en&daysInDestinationFrom=&daysInDestinationTo=&affilid=&children=0&infants=0&flyFrom=49.2-16.61-250km&to=anywhere&featureName=aggregateResults&dateFrom=06/03/2021&dateTo=06/04/2021&typeFlight=oneway&returnFrom=&returnTo=&one_per_date=0&oneforcity=1&wait_for_refresh=0&adults=1&limit=45&partner=skypicker

- documentation of the API and its returned data is here:

https://docs.kiwi.com/search-api/

- if you wish to get destination image, you can get them on 

https://images.kiwi.com/photos/600x330/london_gb.jpg 

(you can use the same ids that are returned from the Flights API mentioned above)

